 # Tuto Django Pokedex
 édité par **Thomas Feutren**
 corrigé par **Martin Bonnefoy**
 


##  Prérequis :
* Un PC avec de préférence Linux ou WSL mais Windows marche aussi 

* **python3**, pour vérifier si bien installé : `python3 --version`

* Un éditeur de code ( genre **VScode** par exemple )



## Objectifs:
* Se familiariser avec le **framework Django** en créant une "mini application" basée sur le thème de **pokemon** : création d'un site web qui **répertorie les pokémons ainsi que leurs caractéristiques**.

* On apprend vraiment les bases, *pas de panique si vous êtes des amateurs*

* Commprendre les arborescences de Django et le fonctionnement du WEB.



## 1) Créer le projet Django
* Créer votre propre dossier de travail :
```bash
cd "Dosser_De_Travail"
mkdir DjangoPokemon
cd DjangoPokemon
```

* Créer un **environnement virtuel**
```bash
python3 -m venv env
```
Ici le nom de notre environnement virtuel est **"env"**
Vous pouvez apercevoir un dossier env qui a été créé ! 

* Maintenant, il faut **activer l'environnement** :
```bash
Linux:
source env/bin/activate
Windows:
./env/Scripts/activate
```
Attention : si vous quittez votre termnial et y revenez, il faudra bien taper cette commande sinon ça marchera pas.

* Une fois l'environnement activé, on peut y installer django: 
```bash
pip install django
```

* On peut également créer un fichier **requirements.txt** qui permet de lister les dépendances nécessaires dans l'environnement pour que tout fonctionne bien :
```bash
pip freeze > requirements.txt
```
ainsi les dépendances sont explicitées dans le fichier requirements.txt :
![alt text](images/requirements.png)

* Créer le projet Django
```bash
django-admin startproject Pokedex
```
Ici le nom du projet est **"Pokedex"**. Vous pouvez remarquer qu'une arborescence s'est créée.

## 2) Démarrer une appli dans notre projet Django
On peut effectuer plusieurs applications dans notre projet django (une sous-section du projet django). Mais ici "on apprend les bases" donc une seule appli suffit !

* Créer l'appli :
```bash
cd Pokedex
python3 manage.py startapp app_pokedex
```

* Exécuter le serveur : ( commande utilisée régulièrement pour avoir une visu sur le site web )
```bash
python3 manage.py runserver 8000
```
Vous pouvez ainsi voir le rendu de votre page en localhost sur http://127.0.0.1:8000/
![alt text](images/fusee.png)

* Gérer les migrations :
```bash
python3 manage.py migrate
```
ainsi un fichier **"db.sqlite3"** à été créé, il contient la base de données de notre appli.

* Installé l'appli dans le projet Django:
il faut ajouter l'appli dans INSTALLED_APPS du fichier `settings.py` :
![alt text](images/settings.png)


## 3) Créer une page WEB

* Modifier `views.py` et ajouter une fonction pour afficher du **html**
```python
from django.http import HttpResponse
from django.shortcuts import render

def main(request):
    return HttpResponse('<h1>Voici mon site de Pokedex</h1>')
```

* Modifier `urls.py` pour **ajouter un url** pour la page principale:
```python
from django.contrib import admin
from django.urls import path

from app_pokedex import views

urlpatterns = [
    path('admin/', admin.site.urls),

    path('main/', views.main)
]
```
Maintenant si vous relancez le serveur en localhost et que vous allez sur l'url : http://127.0.0.1:8000/main vous devez obtenir ceci :

![alt text](images/helloworld.png)



## 4) Utiliser les modèles django
Les modèles dans django c'est comme des classes, ici nous allons créer un **modèle** pour **représenter un pokemon**.

* Ajout d'un modèle Pokemon dans le fichier `models.py` :
```python
from django.db import models

class Pokemon(models.Model):
    name = models.CharField(max_length=100)
    number = models.IntegerField()
    photo = models.ImageField(upload_to='photos', null=True, blank=True)
```
* Il est necessaire de faire une **migration** pour que le **modèle Pokemon** soit bien pris en compte :
```bash
python3 manage.py makemigrations
python3 manage.py migrate
```

* Maintenant il faut créer une **instance** de ce modèle, c'est-à-dire un pokemon, commençons par y ajouter **Pikachu n°25** : 
```bash
python3 manage.py shell
```
Nous voici donc dans le terminal de notre appli django, il suffit de taper les commandes suivantes pour créer notre instance :
```python
from app_pokedex.models import Pokemon
pokemon = Pokemon()
pokemon.name = "Pikachu"
pokemon.number = 25
pokemon.save()
pokemon
# Ctlr + D pour quitter
```
Et voilà ! Votre premier pokemon est inscit dans la base de données, vous pouvez reproduire cette opération pour un autre pokemon :
**Carapuce n°7** par exemple !

* Modifier ainsi la vue `views.py`pour modifier l'affichage :
```python
from django.http import HttpResponse
from django.shortcuts import render

from app_pokedex.models import Pokemon

def main(request):
    pokes = Pokemon.objects.all()
    return HttpResponse(f"""
        <h1>Voici mon site de Pokedex</h1>
        <p>Liste des pokemons dans mon pokedex<p>
        <ul>
            <li>{pokes[0].name}</li>
            <li>{pokes[1].name}</li>
        </ul>""")
```
Maintenant si vous relancez le serveur en localhost et que vous allez sur l'URL : http://127.0.0.1:8000/main vous devez obtenir ceci :
![alt text](images/pokemonlist.png)



## 5) Utiliser les Gabarits
Souvenez-vous de views.py : c'est pas très propre car on a du HTML dans la fonction, c'est un anti-pattern. On va modifier tout ça pour que ce soit plus clean en créant un dossier dédié aux fichiers HTML.

* Dans le dossier `app_pokedex`, créer un dossier `templates`, c'est ici que tout les fichiers html serons stockés.

* Vous pouvez créer un fichier nommé `main.html` et y insérer le code html pour la page principale :
```html
<html>
    <head><title>Pokedex</title></head>
    <body>
        <h1>Voici mon site de Pokedex</h1>
        <p>Liste des pokemons dans mon pokedex</p>
        <ul>
            {% for poke in pokemons %}
            <li>{{ poke.name }} n°{{ poke.number }}</li>
            {% endfor %}
        </ul>
    </body>
</html>
```
Ce sera notre code HTML qui représentera la page d'accueil de notre site WEB.

* Maintenant il faut modifier `views.py`pour que la fonction main retourne le rendu de notre fichier `main.py`, ainsi que l'ajout de import `render` qui permet de lire le fichier HTML.
```python
from django.http import HttpResponse
from django.shortcuts import render

from app_pokedex.models import Pokemon

def main(request):
    pokemons = Pokemon.objects.all()
    return render(request,'main.html',{'pokemons': pokemons})
```

* Et voilà ! Si tout se passe bien vous devez obtenir ceci :
![alt text](images/pokenumber.png)
Pas trop dur n'est-ce pas ? Merci Django !


## 6) Rajouter un peu de style avec CSS 
Maintenant que l'on a notre partie HTML dans un dossier templates, on pourrait également ajouter le CSS pour le style *(couleur, styles de police, espacement, tableaux ...)*

* Commençons par créer un dossier pour les fichiers .css tout comme le html, toujours dans le dossier `app_pokedex`, nommé `static` et créer un fichier `styles.css` à l'interieur.

* Pour `styles.css` je vous épargne les détails, ce n'est pas important, mais ce fichier permet de décrire les styles pour notre site web.
Voici un exemple tout fait de `styles.css` :
```css
body, table {
    margin: 0;
    padding: 0;
}

body {
    font-family: Arial, sans-serif;
    background-color: #f5f5f5;
    text-align: center;
    padding: 20px;
}

h1 {
    color: #333;
    font-size: 36px;
    margin-bottom: 10px;
}

p {
    color: #666; 
    font-size: 18px;
}

table {
    width: 80%;
    margin: 20px auto;
    border-collapse: collapse;
    background-color: #fff;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
}

th {
    background-color: #333;
    color: #fff;
    padding: 10px;
}

td {
    border: 1px solid #ddd;
    padding: 10px;
    font-size: 20px;
}

tr:nth-child(odd) {
    background-color: #f2f2f2;
}
```

* Il faut maintenant ajouter du code dans `main.html` pour que le css s'applique à cette page :
```html
{% load static %}

<html>
    <head>
        <title>Pokedex</title>
        <link rel="stylesheet" href="{% static 'styles.css' %}" />
    </head>
    <body>
        <h1>Voici mon site de Pokedex</h1>
        <p>Liste des pokemons dans mon pokedex</p>
        <table>
            <thead>
                <tr>
                    <th>Nom</th>
                    <th>Numéro</th>
                </tr>
            </thead>
            <tbody>
                {% for poke in pokemons %}
                <tr>
                    <td>{{ poke.name }}</td>
                    <td>{{ poke.number }}</td>
                </tr>
                {% endfor %}
            </tbody>
        </table>
    </body>
</html>
```
On a rajouté une balise `<link>` qui pointe le fichier `styles.css`. Vous remarquez aussi que j'ai modifié les balises pour changer la liste de pokemon en un **tableau**, c'est plus classe pas vrai ?

* Essayer de reload le serveur, puis **rafraichir la page** *(si vous avez un problème, il faut faire ctrl+F5)*, si tout se passe bien vous devriez obtenir ceci :
![alt text](images/css.png)

## 7) Créer un Superuser
Che pas si vous avez remarquez, mais dans le `url.py` il y avait déjà une URL par défaut qui est **"admin/**.
Vous pouvez allez jeter un oeil à cette URL : http://127.0.0.1:8000/admin/

![alt text](images/admin.png)

On nous demande de nous login ! Pas de souci on va créer un **Superuser** pour cela.

* Retournez sur votre terminal et tapez :

```bash
python3 manage.py createsuperuser
```
On vous demande de donner username, email *(pas obligé)* et le password. 
* Une fois cela fait, vous pouvez vous login sur http://127.0.0.1:8000/admin/

* Le problème c'est que notre modèle *Pokemon* ne s'affiche pas ... Pas de panique nous ne l'avons pas encore notifié dans le fichier `admin.py` :
```python
from django.contrib import admin

from app_pokedex.models import Pokemon

admin.site.register(Pokemon)
```
* Si tout se passe bien vous devez voir l'ajout d'un onglet Pokemon qui correspond à notre modèle
* On peut également ajouter une fonction dans `models.py` pour bien afficher les instances créées de notre modèle Pokemon :
 ```python
from django.db import models

class Pokemon(models.Model):
    name = models.CharField(max_length=100)
    number = models.IntegerField()
    photo = models.ImageField(upload_to='photos', null=True, blank=True)

    # Afficher les noms des pokemons dans admin
    def __str__(self):
        return f'{self.name}'
```
* Maintenant on peut ajouter plus facilement des pokemons, les modifier, mais surtout leur ajouter une photo ! N'hésiter pas à ajouter une photo pour Pikachu et Carapuce, et si vous le voulez vous pouvez inscire un nouveau pokemon dans votre pokedex !

* Pour stocker les photos, vous DEVEZ créer un dossier `photos` dans le dossier Pokedex (dans le répertoire où se trouve manage.py), et dans ce dossier photos, vous devez encore créer un dossier `photos` _( jsp pk, c'est chelou, mais sinon ça marche pas ... )_

![alt text](images/modele.png)

* Je vous conseille ce site pour ajouter les pokemons de votre choix : 
https://www.pokepedia.fr/Liste_des_Pok%C3%A9mon_dans_l%27ordre_du_Pok%C3%A9dex_National#Liste_des_Pok%C3%A9mon



## 8) Afficher les images de nos pokemons

* Et oui on aimerait bien ajouter une colonne pour afficher des **photos de nos pokemons**.

* Commençons par ajouter ceci à la fin de `settings.py` :
```python
# à la fin de settings.py
import os
STATICFILES_DIRS = ["static"]
MEDIA_URL = 'photos/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'photos/')
```

* C'est pour faire comprendre que les images que l'on veut lire sont dans le dossier `photos`.

* Modifiez également `main.html` pour ajouter une colonne photo :
```html
{% load static %}

<html>
    <head>
        <title>Pokedex</title>
        <link rel="stylesheet" href="{% static 'styles.css' %}" />
    </head>
    <body>
        <h1>Voici mon site de Pokedex</h1>
        <p>Liste des pokemons dans mon pokedex</p>
        <table>
            <thead>
                <tr>
                    <th>Image</th> 
                    <th>Nom</th>
                    <th>Numéro</th>
                </tr>
            </thead>
            <tbody>
                {% for poke in pokemons %}
                <tr>
                    <td>
                        {% if poke.photo %}
                            <img src="{{ poke.photo.url }}" alt="{{ poke.name }}" width="100" /> 
                        {% else %}
                            None
                        {% endif %}
                    </td>
                    <td>{{ poke.name }}</td>
                    <td>{{ poke.number }}</td>
                </tr>
                {% endfor %}
            </tbody>
        </table>
    </body>
</html>
```

* Pour finir, il faut modifer `urls.py`:
```python
from django.contrib import admin
from django.urls import path

from app_pokedex import views

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),

    path('main/', views.main)
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
    document_root=settings.MEDIA_ROOT)
```

* Et voici le rendu que vous devriez obtenir :

![alt text](images/images.png)




## 9) Ajout attribut "Type" pour le modèle Pokemon

* C'est cool, on a des pokemons avec leur nom, numéro, et photo, mais ça serait mieux avec leur **type ( eau, pierre, feu ...) **
* **FAAAACILE !** Il suffit d'ajouter l'attribut `type` dans notre modèle :
```python
type = models.CharField(max_length=100)
```
* Oui mais non ... Car l'**utilisateur peut saisir plusieurs versions de la même valeur**, par exemple pour le type FEU : **"feu", "Feu', "FEU" ... **

* Du coup pour éviter ce problème, on va avoir besoin d'utiliser la classe imbriquée : c'est une classe définie dans une autre classe.

* Modifiez donc `model.py`:
```python
from django.db import models

class Pokemon(models.Model):
    name = models.CharField(max_length=100)
    number = models.IntegerField()
    photo = models.ImageField(upload_to='photos', null=True, blank=True)

    class Type(models.TextChoices):
        FEU = "FEU",
        EAU = "EAU",
        PLANTE = "PLANTE",
        ELECTRIQUE = "ELECTRIQUE",
        NORMAL = "NORMAL",
        VOL = "VOL",
        COMBAT = "COMBAT",
        POISON = "POISON",
        SOL = "SOL",
        ROCHE = "ROCHE",
    
    type = models.fields.CharField(choices=Type.choices, max_length=50)

    def __str__(self):
       return f'{self.name}'
```

* Maintenant, il faut faire les migrations :

```bash
python3 manage.py makemigrations
```
on va vous demander si vous voulez donner un type par défaut pour les pokemons déja inscit, tapez donc "1", et tapez "FEU" pour que tout les pokemons aient un type par défaut.

* Jetez un coup d'oeil sur **l'url admin**, et vous pouvez remarquer que l'on peut selectionner les types pour les pokémons, **faite le pour chacun de vos pokemon** !

* Enfin, vous pouvez modifiez `main.html` pour **ajouter une colonne "type"** :
```html
{% load static %}

<html>
    <head>
        <title>Pokedex</title>
        <link rel="stylesheet" href="{% static 'styles.css' %}" />
    </head>
    <body>
        <h1>Voici mon site de Pokedex</h1>
        <p>Liste des pokemons dans mon pokedex</p>
        <table>
            <thead>
                <tr>
                    <th>Image</th> 
                    <th>Nom</th>
                    <th>Type</th>
                    <th>Numéro</th>
                </tr>
            </thead>
            <tbody>
                {% for poke in pokemons %}
                <tr>
                    <td>
                        {% if poke.photo %}
                            <img src="{{ poke.photo.url }}" alt="{{ poke.name }}" width="100" /> 
                        {% else %}
                            None
                        {% endif %}
                    </td>
                    <td>{{ poke.name }}</td>
                    <td>{{ poke.type }}</td>
                    <td>{{ poke.number }}</td>
                </tr>
                {% endfor %}
            </tbody>
        </table>
        <p> Attrapez les tous !</p>
    </body>
</html>
```
* Si tout c'est bien passé, vous devez obtenir ceci :

![alt text](images/type.png)

* FELICITATION ! 😎 Ce tuto est terminé, on pourrait évidemment aller plus loin :
    * Création d'un autre modèle pour les Dresseurs
    * Faire une page principale pour se diriger sur d'autres pages secondaires.
    * Faire en sorte que chaque dresseur aient leur propre pokedex
    * Création d'un formulaire pour que les utilisateurs puissent ajouter des pokemons
    * Création de compte pour chaque Dresseur
    * ...

* **Merci d'avoir suivi se tuto** 💛
    
